<?php

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

$id = $_REQUEST['id'];
$pcsprodupdate = $_REQUEST['pcs'];
$qtyprodupdate = $_REQUEST['qty'];

$query_to_get_data = mysqli_query($koneksi, "
  SELECT * 
  FROM daily_input_detail 
  INNER JOIN daily_input ON daily_input_detail.id_daily_input = daily_input.id
  WHERE daily_input_detail.id = '$id' ");

while ($x = mysqli_fetch_array($query_to_get_data)) {

  echo "Id daily_input_detail : ".$id."<br />
     Id daily_input : ".$x['id_daily_input']."<br />
     Pcs update : ".$pcsprodupdate."<br />
     Current PCS daily : ".$x['pcs']."<br />
     pcs update - current pcs daily : ".$recently_pcs = ($pcsprodupdate - $x['pcs'])."<br />
     Qty update : ".$qtyprodupdate."<br />
     Current QTY daily : ".$x['qty']."<br />
     qty update - current QTY daily : ".$recently_qty = ($qtyprodupdate - $x['qty'])."<br />";

  //update pcs and qty daily input detail
  $query1 = "UPDATE daily_input_detail SET pcs = '$pcsprodupdate', qty = '$qtyprodupdate' WHERE id = '$id' ";
  $sql1 = mysqli_query($koneksi, $query1); 

  //----------------------------------------------------------------------------------------------------------------
  //query2 = Get total paid by working hour, get total qty per day by date to count total_packing_cost / day
  $get_total_paid = mysqli_query($koneksi, "SELECT total_time_in_sec, total_paid FROM daily_input WHERE id = '".$x['id_daily_input']."' ");
  while($a = mysqli_fetch_array($get_total_paid)){
      $total_paid_daily = $a['total_paid'];
      $total_time_in_sec = $a['total_time_in_sec'];
  }

  $sql = mysqli_query($koneksi, "
    SELECT SUM(daily_input_detail.qty) AS total_qty
    FROM daily_input_detail
    INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
    WHERE daily_input_detail.id_daily_input = '".$x['id_daily_input']."' AND daily_input.date = '".$x['date']."'
    ");

  while($totalqtydaily = mysqli_fetch_array($sql)){
    echo "qty total by date and ID daily_input = ".$totalqtydaily['total_qty'];
    $totalqtydailytocount = $totalqtydaily['total_qty'];
  }

  $to_int = (int) $total_paid_daily;
  echo "<br> total paid daily = ".$to_int."<br>";
  $total_paid_to_insert = round($to_int/$totalqtydailytocount, 2);
  echo "Total packing cost = ".$total_paid_to_insert."<br>";

  $total_item_hour = round($totalqtydailytocount / ($total_time_in_sec / 3600), 2);
  //echo $total_item_hour;

  $query2 = "UPDATE daily_input SET total_packing_cost = '$total_paid_to_insert', total_item_hour = '$total_item_hour' WHERE id = '".$x['id_daily_input']."' ";
  $sql2 = mysqli_query($koneksi, $query2);

  //----------------------------------------------------------------------------------------------------------------
  //query3 = Update PCS and QTY to table import_result
  $sqlgetpcsandqty = mysqli_query($koneksi, "
    SELECT pcs, qty FROM import_result
    WHERE fnsku = '".$x['fnsku']."'
    ");
  while ($updatepcsandqty = mysqli_fetch_array($sqlgetpcsandqty)) {
    $pcsawal = $updatepcsandqty['pcs'];
    $qtyawal = $updatepcsandqty['qty'];
  }

  //$pcsakhir = $pcsawal + $pack; //commented because this is only show current pcs in daily input detail not update current pcs + input pcs
  echo "PCS Awal product = ".$pcsawal."<br>";
  // echo "PCS Akhir = ". $pcsakhir."<br>";
  echo "QTY Awal product = ".$qtyawal."<br>";

  if($recently_pcs > 0){
    $pcsakhir = $pcsawal + $recently_pcs;
  } else {
    $pcsakhir = $recently_pcs + $pcsawal;
  }

  if($recently_qty > 0){
    $qtyakhir = $qtyawal + $recently_qty;
  } else {
    $qtyakhir = $recently_qty + $qtyawal;
  }

  echo "PCS Akhir = ".$pcsakhir."<br>";
  echo "QTY Akhir = ".$qtyakhir."<br>";
  echo "FNSKU = ".$x['fnsku'];

  $query3 = "UPDATE `import_result` SET `pcs`='$pcsakhir', `qty`='$qtyakhir' WHERE fnsku = '".$x['fnsku']."' ";
  $sql3 = mysqli_query($koneksi, $query3);

  if($sql1 && $sql2 && $sql3) {
      //echo "<script type='text/javascript'>document.location.href = 'daily_input_detail_update.php?id=$id';</script>";
      echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=".$x['id_daily_input']."';</script>";
      //echo "<script>alert('QTY Has Been Update!!!');</script>";
  } else {
      //echo "<script type='text/javascript'>document.location.href = 'daily_input_detail_update.php?id=$id';</script>";
      echo "<script type='text/javascript'>document.location.href = 'daily_input_detail.php?id=".$x['id_daily_input']."';</script>";
      //echo "<script>alert('QTY Failed To Update!!!');</script>";      
  }

} //end while query_to_get_data daily input and daily input detail
?>