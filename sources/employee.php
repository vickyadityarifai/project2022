<?php

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Inventory App
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>
<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="../index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Inventory App</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="../index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>        
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white active bg-gradient-info' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>                
              ";
          } else {
              echo "";
          }
        ?>
<!--         <li class="nav-item">
          <a class="nav-link text-white" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Employee</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Time</span>
          </a>
        </li> -->
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Template</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Template</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<?php 
  if(isset($_GET['alert'])){
    if($_GET['alert'] == "success"){
      echo "<div class='alert alert-success text-white' role='alert'>
              <strong>Success!</strong> Data has been saved !!
            </div>";
    } else {
      echo "<div class='alert alert-danger text-white' role='alert'>
      <strong>Failed!</strong> Data has not saved !!
  </div>";
    }
  }
?>

<div class="row">
<div class="col-12">
  <div class="card my-4">
    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
      <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
        <div><h6 class="text-white text-capitalize ps-3" align="left">Employees Table | <a href="employee_add.php"><i class="material-icons text-sm">add</i>Add Employee</a></h6></div>
      </div>
    </div>
    <div class="card-body px-0 pb-2">
      <div class="table-responsive p-0">
        <table class="table align-items-center mb-0">
          <thead>
            <tr>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Full Name</th>
              <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Department</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Privilege</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Status</th>
              <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Detail/Edit</th>
            </tr>
          </thead>
          <tbody>
            <?php

            include 'conn.php';
            $no=1;
            $data = mysqli_query($koneksi,"select * from employee");
            while($d = mysqli_fetch_array($data)){

            ?>
            <tr>
              <td>
                <div class="d-flex px-2 py-1">
                  <div class="d-flex flex-column justify-content-center">
                    <h6 class="mb-0 text-sm"><?php echo $d['last_name'] .' '. $d['first_name']; ?></h6>
                    <p class="text-xs text-secondary mb-0"><?php echo $d['email']; ?></p>
                  </div>
                </div>
              </td>
              <td>
                <p class="text-xs font-weight-bold mb-0">Staff</p>
                <p class="text-xs text-secondary mb-0">
                	<?php if ($d['id_department'] == "1"){ 
                		echo "Human Resources";	} 
                		elseif ($d['id_department'] == "2"){ 
                		echo "Information Technology";	}
                		elseif ($d['id_department'] == "3"){ 
                		echo "Accounting and Finance";	} 
                		elseif ($d['id_department'] == "4"){ 
                		echo "Marketing";	}
                		elseif ($d['id_department'] == "5"){ 
                		echo "Research and Development";	}
                		else { echo "Production"; } ?>
                </p>
              </td>
              <td class="align-middle text-center">
                <span class="text-secondary text-xs font-weight-bold">
                	<?php if ($d['role_id'] == "1"){ 
                		echo "Manager";	} 
                		else { echo "User"; } ?>
                </span>
              </td>
              <td class="align-middle text-center text-sm">
                <?php if ($d['employee_status'] == "11"){ 
                    echo "<span class='badge badge-sm bg-gradient-success'>Active</span>";
                    } else { 
                    echo "<span class='badge badge-sm bg-gradient-secondary'>Inactive</span>"; } 
                ?>
              </td>
              <td class="align-middle text-center">
                <a href="employee_detail.php?id=<?php echo $d['id']; ?>" class="text-secondary font-weight-bold text-xs" data-toggle="tooltip" data-original-title="Detail user">
                  Detail
                </a>
              </td>
            </tr>
            <?php 
            }
            ?>            
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<?php
include '../pages/footer.html';
?>