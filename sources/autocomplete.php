<?php
include "conn.php"; // include file koneksi
$searchkey = $_GET['term']; // menangkap kiriman data dari inputan yang dimasukan

$query = "
      SELECT fnsku, title, pcs, qty
			FROM   import_result s1
			WHERE  id=(SELECT MIN(s2.id)
           				FROM import_result s2
           				WHERE s1.fnsku = s2.fnsku)
       				AND fnsku LIKE '%".$searchkey."%' ORDER BY fnsku ASC
          "; //perintah query sql untuk menampilkan data pada tabel import_result dengan operator SQL = LIKE

$pencarian =mysqli_query($koneksi, $query); // query dieksekusi

// data ditampilkan dengan menggunakan perulangan
while ($row = mysqli_fetch_array($pencarian)) {
    $data[] = $row['fnsku']." | ".$row['title']." | ".$row['pcs']." | ".$row['qty'];
    // $data[] = mb_convert_encoding(
    // 	$row['fnsku'].$row['title'].$row['pcs'].$row['qty'], 'UTF-8', 'UTF-8'
    // 	);
	// $data = [
 // 	   	"fnsku"	=>	$row['fnsku'],
 // 	   	"title"	=>	$row['title'], 	   	
 // 	   	"pcs"	=>  $row['pcs'],
 // 	    "qty"   =>  $row['qty']
 // 	   ];    
}
//$print = json_encode($data, JSON_INVALID_UTF8_IGNORE);
$print = json_encode($data, JSON_PRETTY_PRINT|JSON_PRESERVE_ZERO_FRACTION);
//echo "<div class='p-2' style='background-color: linear-gradient(195deg, #49a3f1 0%, #1A73E8 100%); color: #49a3f1;'><u><b>".$print."</b></u></div>";
echo $print;
// $print = json_decode($data, false);
// echo $print->fnsku;
// echo $print->title;
// echo $print->current_pcs;
// echo $print->current_qty;
//$data['name'] = mb_convert_encoding($data['name'], 'UTF-8', 'UTF-8');
?>