<?php 

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include '../pages/head.html';
include '../pages/navbar.html';
include 'conn.php';
?>
<div class="container-fluid py-4">

<div class="row">
<div class="col-12">
  <div class="card my-4">
    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
      <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
        <div><h6 class="text-white text-capitalize ps-3" align="left">Employee Detail | <a href="employee.php"><i class="material-icons text-sm">arrow_back</i>Back</a></h6></div>
      </div>
    </div>

<?php
$id=mysqli_real_escape_string($koneksi, $_GET['id']);
$det=mysqli_query($koneksi, "
	SELECT * 
	FROM employee 
	INNER JOIN department on employee.id_department = department.id 
	INNER JOIN users on employee.first_name = users.username AND employee.email = users.email
	WHERE employee.id='$id'")or die(mysqli_error());
while($d=mysqli_fetch_array($det)){
?>

    <div class="card-body px-0 pb-2">
    	<div class="table-responsive p-0">
		<form action="employee_detail_save.php" method="post" enctype="multipart/form-data">
		    <div>
		    	<input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
		    </div>
		    <div class="input-group input-group-outline my-3">	
		      <input type="text" name="first_name" class="form-control" value="<?php echo $d['first_name'] ?>">
		    </div>
		    <div class="input-group input-group-outline my-3">
		      <input type="text" name="last_name" class="form-control" value="<?php echo $d['last_name'] ?>">
		    </div>
		    <div class="input-group input-group-outline my-3">
		      <input type="email" name="email" class="form-control" value="<?php echo $d['email'] ?>">
		    </div>
		    <div class="input-group input-group-outline my-3">
		      <input type="password" name="password" class="form-control" value="<?php echo $d['password'] ?>">
		    </div>		    
		    <div class="input-group input-group-outline my-3">
		      <input type="number" name="phone" class="form-control" value="<?php echo $d['phone'] ?>">
		    </div>
		    <div class="input-group input-group-outline my-3">
		    <label class="form-label">Privilege</label><br>
			  </div>
			  <div class="form-check mb-3">
								  	
				  <input class="form-check-input" type="radio" name="role_id" id="1" value="1" <?php if ($d['role_id']=="1") echo "checked";?>> <label class="custom-control-label" for="1">Manager</label><br>
				  <input class="form-check-input" type="radio" name="role_id" id="2" value="2" <?php if ($d['role_id']=="2") echo "checked";?>> <label class="custom-control-label" for="2">User</label>
				  
				</div>
		    <div class="input-group input-group-outline my-3">
		    <label class="form-label">Department</label><br>
		    </div>
				<div class="input-group input-group-outline my-3">
					<select class="form-control" id="id_department" name="id_department">
						<option disabled selected> - Select Department - </option>
					
						<option <?php if ($d['id_department'] == "1"): ?> selected="selected" <?php endif; ?> value="1">Human Resources</option>
						<option <?php if ($d['id_department'] == "2"): ?> selected="selected" <?php endif; ?> value="2">Information Technology</option>
						<option <?php if ($d['id_department'] == "3"): ?> selected="selected" <?php endif; ?> value="3">Accounting and Finance</option>
						<option <?php if ($d['id_department'] == "4"): ?> selected="selected" <?php endif; ?> value="4">Marketing</option>
						<option <?php if ($d['id_department'] == "5"): ?> selected="selected" <?php endif; ?> value="5">Research and Development</option>
						<option <?php if ($d['id_department'] == "6"): ?> selected="selected" <?php endif; ?> value="6">Production</option>
					
					</select>
				</div>
		    <div class="input-group input-group-outline my-3">
		    <label class="form-label">Status</label><br>
			</div>
				<div class="input-group input-group-outline my-3">
					<select class="form-control" id="employee_status" name="employee_status">
						<option disabled selected> - Select Employee Status - </option>
						<option <?php if ($d['employee_status'] == "11"): ?> selected="selected" <?php endif; ?> value="11">Active</option>
						<option <?php if ($d['employee_status'] == "12"): ?> selected="selected" <?php endif; ?> value="12">Not Active</option>
					</select>
				</div>				
		    <div class="input-group input-group-outline my-3">
		      <input type="number" name="rate" class="form-control" value="<?php echo $d['rate'] ?>">
		    </div>
			<div class="footer">
				<a href="employee.php"><input type="cancel" class="btn btn-success" value="Cancel"></a>
				<input type="submit" class="btn btn-info" value="Save">
			</div>
		</form>
		
		</div>
	</div>
<?php
}
?>  
	</div>
</div>
</div>
<?php include '../pages/footer.html'; ?>