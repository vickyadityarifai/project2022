<?php 

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Inventory App
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
</head>
<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="../index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Inventory App</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="../index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white active bg-gradient-info" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>        
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>                
              ";
          } else {
              echo "";
          }
        ?>
<!--         <li class="nav-item">
          <a class="nav-link text-white" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Employee</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Time</span>
          </a>
        </li> -->
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Daily Input</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Daily Input</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<?php 
  if(isset($_GET['alert'])){
    if($_GET['alert'] == "success"){
      echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert'>
              <span><strong>Success!</strong> Data has been saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
    } elseif ($_GET['alert'] == "successdelete") {
      echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert'>
              <span><strong>Success!</strong> Data has been delete !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
    } else {
      echo "<div class='alert alert-danger alert-dismissible text-white fade show my-4' role='alert'>
            <span><strong>Failed!</strong> Data has not saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>            
            </div>";
    }
  }
?>
<div class="row">
<div class="col-12">
  <div class="card my-4">
    <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
      <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
        <div><h6 class="text-white text-capitalize ps-3" align="left">Daily Input Record | <a href="daily_input_add.php"><i class="material-icons text-sm">add</i>Add Daily Input</a></h6></div>
      </div>
    </div>
      <table class="table align-items-center mb-0">
        <thead>
          <tr>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No.</th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Working Date</th>
            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Employee Name</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">Start Time</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">End Time</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">Rate / Hour</th>            
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">Total Time</th>
            <?php 
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>Total Paid</th>";
              } else {
                echo "";
              }
            ?>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1">QTY</th>
            <?php 
              if ($_SESSION['role_id'] == '1'){
                echo "<th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>PC / Item</th>
                      <th class='text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-1'>Item / Hour</th>";
              } else {
                echo "";
              }
            ?>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Details</th>
            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Delete</th>            
          </tr>
        </thead>
        <tbody>
          <?php

          include 'conn.php';
          $no=1;
          $data = mysqli_query($koneksi,"
            SELECT employee.first_name, employee.last_name, employee.rate, daily_input.id, daily_input.date, daily_input.start_time, daily_input.endtime, daily_input.total_time_in_sec, daily_input.total_paid, daily_input.total_packing_cost, daily_input.total_item_hour
            FROM employee
            INNER JOIN daily_input
            ON employee.id = daily_input.id_employee
            ORDER BY daily_input.id DESC
            ");
          while($d = mysqli_fetch_array($data)){

          ?>
        <tr>
          <td>
            <p class="text-xs font-weight-normal mb-0 ps-3"><?php echo $no++; ?></p>
          </td>          
          <td>
            <div class="d-flex align-items-center">
              <p class="text-xs font-weight-normal mb-0"><?php echo $d['date']; ?></p>
            </div>
          </td>
          <td>
            <div class="d-flex">
              <div class="my-auto">
                <h6 class="mb-0 text-xs"><?php echo $d['last_name'].' '.$d['first_name']; ?></h6>
              </div>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-4"><?php echo $d['start_time'] ?></p>
              </span>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-4"><?php echo $d['endtime'] ?></p>
              </span>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-4"><?php echo "$".$d['rate'] ?></p>
              </span>
            </div>
          </td>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-4">
                  <?php 
                    $time = $d['total_time_in_sec']; 
                    $hour = round(($time / 3600)); //hour
                    $minute = round(($time / 60) % 60); //minute                
                    if ($time > 60 AND $time < 3600) {
                      echo $timeex = "0 H ".($minute % 60)." m";
                    } else if ($time > 3600 AND $time < 24*60*60) {
                      echo $timeex = ($hour % 24)." H ".($minute % 60)." m";
                    }                    
                  ?>
                </p>
              </span>
            </div>
          </td>          
          
          <?php
            if ($_SESSION['role_id'] == '1'){ 
              echo "
              <td>
                <div class='d-flex align-items-center'>
                  <span class='text-dark text-xs'>
                    <p class='text-xs font-weight-normal mb-0 ps-4'>";
              
              echo "$".$d['total_paid'];

              echo "</p>
                  </span>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
          <td>
            <div class="d-flex align-items-center">
              <span class="text-dark text-xs">
                <p class="text-xs font-weight-normal mb-0 ps-2">
                  <?php 
                    $qty_display = 0;
                    // $qty_old = "SELECT daily_input.date, daily_input_detail.fnsku, import_result.title, daily_input_detail.pcs, daily_input_detail.qty, daily_input.total_packing_cost
                    // FROM daily_input_detail
                    // INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                    // INNER JOIN import_result ON import_result.fnsku = daily_input_detail.fnsku
                    // WHERE daily_input.id = '".$d['id']."'
                    // GROUP BY daily_input.date ";
                    $qty = "SELECT SUM(daily_input_detail.qty) AS total_qty
                        FROM daily_input_detail
                        INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
                        WHERE daily_input_detail.id_daily_input = '".$d['id']."' ";
                    $queryqty = mysqli_query($koneksi, $qty);
                    while ($qtyshow = mysqli_fetch_array($queryqty)) {
                      //$qty_display = $qtyshow['qty'];
                      $qty_display = $qtyshow['total_qty'];
                    }
                    
                    if ($qty_display > 0) {
                      echo $qty_display;
                    } else {
                      echo "<b style='color:red;'>0</b>";
                    }
                  ?>
                </p>
              </span>
            </div>
          </td>
          <?php
            if ($_SESSION['role_id'] == '1'){ 
              echo "
              <td>
                <div class='d-flex'>
                  <div class='my-auto'>
                    <h6 class='mb-0 text-xs ps-4'>";
              echo "$".$d['total_packing_cost'];
              echo "</h6>
                  </div>
                </div>
              </td>
              <td>
                <div class='d-flex'>
                  <div class='my-auto'>
                    <h6 class='mb-0 text-xs ps-4'>";
              echo $d['total_item_hour'];
              echo "</h6>
                  </div>
                </div>
              </td>";
            } else {
              echo "";
            }
          ?>
          <td class="align-middle">
            <div>
              <h6 class="text-white text-capitalize ps-4 my-1" align="left">
                <a href="daily_input_detail.php?id=<?php echo $d['id']; ?>"><i class="material-icons text-sm">settings</i>Detail
                </a>
              </h6>
            </div>
          </td>
          <td class="align-middle">
            <div>
              <h6 class="text-white text-capitalize ps-3 my-1 remove" align="left">
                <!--<a href="daily_input_delete.php?id=<?php echo $d['id'] ?> "><i class="material-icons text-sm">delete</i>Delete</a>
                --><a href="javascript:deleteData('<?php echo $d['id'] ?>')"><i class="material-icons text-sm">delete</i>Delete</a>
              </h6>
            </div>
          </td>          
        </tr>
          <?php 
          }
          ?>            
        </tbody>
      </table>
        <i>*If QTY is <i style='color:red;'>0</i>, hurry up add daily input detail on to that daily input.</i>
		  </div>
	  </div>
  </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- <script type="text/javascript">
    $(".remove").click(function(){
      var id = $(this).parents("tr").attr("id");

      if(confirm('Are you sure to remove this record ?'))
      {
        $.ajax({
           url: 'daily_input_delete.php',
           type: 'GET',
           data: {id: id},
           error: function() {
              alert('Something is wrong');
           },
           success: function(data) {
              $("#"+id).remove();
              alert('Record removed successfully');  
           }
        });
      }
      return false;
    });
</script> -->
<script type="text/javascript">
  function deleteData(id){
    if (confirm("Do You want to Delete this Record ?")){
      window.location.href = 'daily_input_delete.php?id=' + id;
    }
  }
</script>

<?php include '../pages/footer.html'; ?>