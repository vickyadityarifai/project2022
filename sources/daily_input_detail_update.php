<?php 

session_start();
// do check
if (!isset($_SESSION["username"])) {
    header("location: login.php");
    exit; // prevent further execution, should there be more code that follows
}

include 'conn.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="../image/png" href="../assets/img/favicon.png">
  <title>
    Inventory App
  </title>
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
  <!-- Nucleo Icons -->
  <link href="../assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="../assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <!-- Material Icons -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
  <!-- CSS Files -->
  <link id="pagestyle" href="../assets/css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
  <script>
    $(document).ready(function(){
      $("form").submit(function(){
        alert("Submitted");
      });
    });
  </script>
</head>
<body class="g-sidenav-show  bg-gray-200">
  <aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3   bg-gradient-dark" id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="../index.php">
        <img src="../assets/img/logo-ct.png" class="navbar-brand-img h-100" alt="main_logo">
        <span class="ms-1 font-weight-bold text-white">Inventory App</span>
      </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link text-white" href="../index.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">dashboard</i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li>
          <a class="nav-link text-white active bg-gradient-info" href="daily_input.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">add_task</i>
            </div>
            <span class="nav-link-text ms-1">Daily Input</span>
          </a>          
        </li>        
        <?php
          if ($_SESSION['role_id'] == '1'){
              echo "
              <li class='nav-item'>
                <a class='nav-link text-white' href='employee.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>table_view</i>
                  </div>
                  <span class='nav-link-text ms-1'>Employee</span>
                </a>
              </li> 
              <li class='nav-item'>
                <a class='nav-link text-white' href='products.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>inventory</i>
                  </div>
                  <span class='nav-link-text ms-1'>Products</span>
                </a>
              </li>
              <li class='nav-item mt-3'>
                <h6 class='ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8'>Report pages</h6>
              </li>        
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Employee</span>
                </a> 
              </li>
              <li class='nav-item'>
                <a class='nav-link text-white' href='reportpage_time.php'>
                  <div class='text-white text-center me-2 d-flex align-items-center justify-content-center'>
                    <i class='material-icons opacity-10'>library_add</i>
                  </div>
                  <span class='nav-link-text ms-1'>Report By Time</span>
                </a> 
              </li>                
              ";
          } else {
              echo "";
          }
        ?>
<!--         <li class="nav-item">
          <a class="nav-link text-white" href="report.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Employee</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-white" href="report_old.php">
            <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="material-icons opacity-10">library_add</i>
            </div>
            <span class="nav-link-text ms-1">Report By Time</span>
          </a>
        </li> -->
      </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0 ">
      <div class="mx-3">
        <a class="btn bg-gradient-info mt-4 w-100" href="logout.php" type="button">Log Out</a>
      </div>
    </div>    
  </aside>
  <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" data-scroll="true">
      <div class="container-fluid py-1 px-3">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
            <li class="breadcrumb-item text-sm"><a class="opacity-5 text-dark" href="javascript:;">Pages</a></li>
            <li class="breadcrumb-item text-sm text-dark active" aria-current="page">Daily Input</li>
          </ol>
          <h6 class="font-weight-bolder mb-0">Daily Input</h6>
        </nav>
        <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
          <div class="ms-md-auto pe-md-3 d-flex align-items-center">
            <div class="input-group input-group-outline">
              <label class="form-label">Type here...</label>
              <input type="text" class="form-control">
            </div>
          </div>
          <ul class="navbar-nav  justify-content-end">
            <li class="nav-item d-flex align-items-center">
              <a href="#" class="nav-link text-body font-weight-bold px-0">
                <i class="fa fa-user me-sm-1"></i>
                <span class="d-sm-inline d-none">
                  <?php echo $_SESSION['username']; ?>
                </span>
              </a>
            </li>
            <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </a>
            </li>
            <li class="nav-item px-3 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0">
                <i class="fa fa-cog fixed-plugin-button-nav cursor-pointer"></i>
              </a>
            </li>
            <li class="nav-item dropdown pe-2 d-flex align-items-center">
              <a href="javascript:;" class="nav-link text-body p-0" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="fa-solid fa-right-from-bracket cursor-pointer"></i>
                <i class="fa fa-bell cursor-pointer"></i>
              </a>
              <ul class="dropdown-menu  dropdown-menu-end  px-2 py-3 me-sm-n4" aria-labelledby="dropdownMenuButton">
                <li class="mb-2">
                  <a class="dropdown-item border-radius-md" href="logout.php">
                    <div class="d-flex py-1">
                      <div class="d-flex flex-column justify-content-center">
                        <h6 class="text-sm font-weight-normal mb-1">
                          <span>Log Out Now</span>
                        </h6>
                        <p class="text-xs text-secondary mb-0">
                          <i class="fa fa-clock me-1"></i>
                          <div id="current_date"></p>
                          <script>
                          document.getElementById("current_date").innerHTML = Date();
                          </script>                          
                        </p>
                      </div>
                    </div>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- End Navbar -->

<div class="container-fluid py-4">

<?php 
include 'conn.php';
$id=mysqli_real_escape_string($koneksi,$_GET['id']);
$det=mysqli_query($koneksi,"
  SELECT * FROM daily_input_detail 
  INNER JOIN import_result s1 ON s1.fnsku = daily_input_detail.fnsku 
  INNER JOIN daily_input ON daily_input.id = daily_input_detail.id_daily_input
  WHERE daily_input_detail.id='$id' 
  AND s1.id=(
    SELECT MIN(s2.id) 
    FROM import_result s2 
    WHERE s1.fnsku = s2.fnsku) ")or die(mysqli_error());
while($d = mysqli_fetch_array($det)){
?>

<h5>Update QTY Daily Input FNSKU = (<?php echo $d['fnsku']; ?>)</h5>
 
<button type="button" class="btn btn-info btn-lg"><a href="daily_input_detail.php?id=<?php echo $d['id_daily_input'] ?>">Back</a></button>

<?php 
  if(isset($_GET['alert'])){
    if($_GET['alert'] == "success"){
      echo "<div class='alert alert-success alert-dismissible text-white fade show my-4' role='alert'>
              <span><strong>Success!</strong> Data has been saved !!</span>
              <button type='button' class='btn-close' data-bs-dismiss='alert' aria-label='Close'>
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>";
    } else {
      echo "<div class='alert alert-danger text-white' role='alert'>
            <strong>Failed!</strong> Data has not saved !!
            </div>";
    }
  }
?>

<div class="row">
  <div class="col-md-7 mt-4">
    <div class="card">
      <div class="card-header pb-0 px-3">
      </div>
      <div class="card-body pt-4 p-3">
        <ul class="list-group">
          <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
            <form method="post" action="daily_input_detail_update_save.php">
            <div class="d-flex flex-column">
              <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
              <input type="hidden" name="id_daily_input" value="<?php echo $_GET['id_daily_input']; ?> ">
              <input type="hidden" name="current_pcs" value="<?php echo $_GET['pcs']; ?> ">
              <input type="hidden" name="current_qty" value="<?php echo $_GET['qty']; ?> ">

              <table>
                <tr><span class="text-xs">Title &nbsp;&nbsp;&nbsp;&nbsp;: <span class="text-dark font-weight-bold ms-sm-2"><?php echo $d['title']; ?></span></span></tr>
                <!-- <span class="text-xs">Pack &nbsp;&nbsp;&nbsp;: <span class="text-dark ms-sm-2 font-weight-bold"><?php echo $d['pcs']; ?></span></span> -->

                <?php
                  $current_pcs_daily = mysqli_query($koneksi, "SELECT * FROM daily_input_detail WHERE id = '$id' ");
                  while ($e = mysqli_fetch_array($current_pcs_daily)) {
                    
                ?>
                  <span class="text-xs">PCS &nbsp;&nbsp;&nbsp;&nbsp;: <span class="text-dark ms-sm-2 font-weight-bold"><input type="number" name="pcs" value="<?php echo $e['pcs']; ?>"></span></span>      
                  <span class="text-xs">QTY &nbsp;&nbsp;&nbsp;&nbsp;: <span class="text-dark ms-sm-2 font-weight-bold"><input type="number" name="qty" value="<?php echo $e['qty']; ?>"></span></span>
                <?php
                  }
                ?>

              </table>
            </div>
            <br>
            <input type="submit" class="btn btn-info" value="Save">
            </form>
          </li>
        </ul>
        <div class="ms-auto text">
          <i>*When update is <b>Successful</b>, You will automatically back to previous page.</i>
        </div>
      </div>
    </div>
  </div>
</div>

<?php 
}
?>

</div>
<?php include '../pages/footer.html'; ?>